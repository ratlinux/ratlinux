# ratLinux

ratLinux is an **Arch-Based Linux distribution**, Which focuses on **Performance** and **Customization**


### How to install ratLinux?

 > ratLinux is currently in development, when the installer will be finished we will give installation instructions

ratLinux can be installed from any linux distribution, just use the instruction that suitable for you.

---

### Design
ratLinux currently doesn't have any design guidelines or logo, so for now it's a placeholder. the accent color is probably going to be `#D91E36`

---

### How to contribute?

Just make an issue!

 Issues is the best way to know if there is a bug, or for requesting a feature

---

If you want to contribute code, you can make a merge request, and it will be reviewed and pushed to testing branch, and _maybe_ will be merged to 

## Credits

[Arch Linux](https://archlinux.org/) - for the base of ratLinux
